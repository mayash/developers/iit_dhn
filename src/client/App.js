import React, { Component } from "react";

import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme } from "@material-ui/core/styles";

import AppRoutes from './AppRoutes';

class App extends Component {
  render() {
    const theme = createMuiTheme({
      palette: {
        primary: {
          light: "#757ce8",
          main: "#039BE5",
          dark: "#002884",
          contrastText: "#fff"
        },
        secondary: {
          light: "#ff7961",
          main: "#ff5252",
          dark: "#ba000d",
          contrastText: "#000"
        }
      }
    });

    return (
      <React.Fragment>
        <CssBaseline />
          <MuiThemeProvider theme={theme}>
            <AppRoutes/>
        </MuiThemeProvider>
      </React.Fragment>
    );
  }
}

export default App;